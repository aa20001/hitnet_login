use std::fs;
use std::env;
use std::os::unix::fs::PermissionsExt;
use std::process::Command;
use std::io::Write;
use std::path::Path;

static SERVICE_FILE_PATH: &str = "/etc/systemd/system/hitnet_login.service";
static TIMER_FILE_PATH: &str = "/etc/systemd/system/hitnet_login.timer";

pub fn setup(user_id: &str, password: &str) -> Result<(), ()> {
    let user = env::var("USER").expect("failed to get current user");
    println!("configurating systemd as {user}");

    let service_str = include_str!("../resources/hitnet_login.service").to_string();
    let service_str = service_str.replace("{password}", password);
    let service_str = service_str.replace("{user_id}", user_id);
    let service_str = service_str.replace("{user}", &user);
    let service_str = service_str.replace("{binary}", env::current_exe().unwrap().to_str().expect("failed to get executable path"));

    let service_file = fs::File::create(SERVICE_FILE_PATH);
    if let Err(err) = service_file {
        eprintln!("{err}");
        eprintln!("try this command with prefix 'sudo ' if you not superuser");
        return Err(())
    }
    let mut service_file = service_file.unwrap();
    let metadata = service_file.metadata().expect("metadata read error");
    let mut permission = metadata.permissions();
    permission.set_mode(0o700);
    service_file.set_permissions(permission).expect("permission write error");

    writeln!(service_file, "{}", service_str).expect("service file write failed");


    let timer_str = include_str!("../resources/hitnet_login.timer").to_string();
    let timer_file = fs::File::create(TIMER_FILE_PATH);
    if let Err(err) = timer_file {
        eprintln!("{err}");
        eprintln!("try this command with prefix 'sudo ' if you not superuser");
        return Err(())
    }
    let mut timer_file = timer_file.unwrap();
    let metadata = timer_file.metadata().expect("metadata read error");
    let mut permission = metadata.permissions();
    permission.set_mode(0o700);
    timer_file.set_permissions(permission).expect("permission write error");

    writeln!(timer_file, "{}", timer_str).expect("timer file write failed");

    
    let _reload_cmd = Command::new("systemctl").arg("daemon-reload").status();
    
    let _service_enable_cmd = Command::new("systemctl").arg("enable").arg("hitnet_login.service").status().expect("hitnet_login.service enable failed");
    let _service_start_cmd = Command::new("systemctl").arg("start").arg("hitnet_login.service").status().expect("hitnet_login.service start failed");

    let _timer_enable_cmd = Command::new("systemctl").arg("enable").arg("hitnet_login.timer").status().expect("hitnet_login.timer enable failed");
    let _timer_start_cmd = Command::new("systemctl").arg("start").arg("hitnet_login.timer").status().expect("hitnet_login.timer start failed");

    Ok(())
}

pub fn down() -> Result<(), ()> {
    let _timer_enable_cmd = Command::new("systemctl").arg("disable").arg("hitnet_login.timer").status().expect("hitnet_login.timer enable failed");
    let _timer_start_cmd = Command::new("systemctl").arg("stop").arg("hitnet_login.timer").status().expect("hitnet_login.timer start failed");
    if Path::new(TIMER_FILE_PATH).exists() {
        fs::remove_file(TIMER_FILE_PATH).expect("failed to remove timer");
    }
    
    let _service_enable_cmd = Command::new("systemctl").arg("disable").arg("hitnet_login.service").status().expect("hitnet_login.service enable failed");
    let _service_start_cmd = Command::new("systemctl").arg("stop").arg("hitnet_login.service").status().expect("hitnet_login.service start failed");
    if Path::new(SERVICE_FILE_PATH).exists() {
        fs::remove_file(SERVICE_FILE_PATH).expect("failed to remove service");
    }

    Ok(())
}