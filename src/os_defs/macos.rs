use std::fs;
use std::os::unix::fs::PermissionsExt;
use std::process::Command;
use std::io::Write;
use std::path::Path;

static PLIST_PATH: &str = "/Library/LaunchDaemons/io.gitlab.aa20001.hitnet_login.plist";

fn make_exec_content(user_id: &str, password: &str) -> Result<String, xml::writer::Error> {
    use xml::writer::{EmitterConfig, XmlEvent};
    use std::env;

    let mut out = Vec::new();
    let mut e = EmitterConfig::new().perform_indent(true).write_document_declaration(false).create_writer(&mut out);
    e.write(XmlEvent::start_element("string"))?;
    e.write(XmlEvent::characters(env::current_exe().unwrap().to_str().unwrap()))?;
    e.write(XmlEvent::end_element())?;

    e.write(XmlEvent::start_element("string"))?;
    e.write(XmlEvent::characters("login"))?;
    e.write(XmlEvent::end_element())?;

    e.write(XmlEvent::start_element("string"))?;
    e.write(XmlEvent::characters("-u"))?;
    e.write(XmlEvent::end_element())?;

    e.write(XmlEvent::start_element("string"))?;
    e.write(XmlEvent::characters(user_id))?;
    e.write(XmlEvent::end_element())?;

    e.write(XmlEvent::start_element("string"))?;
    e.write(XmlEvent::characters("-p"))?;
    e.write(XmlEvent::end_element())?;

    e.write(XmlEvent::start_element("string"))?;
    e.write(XmlEvent::characters(password))?;
    e.write(XmlEvent::end_element())?;


    let xml_data = String::from_utf8(out).expect("xml encoding error");
    return Ok(xml_data)
}

pub fn setup(user_id: &str, password: &str) -> Result<(), ()> {
    let service_str = include_str!("../resources/io.gitlab.aa20001.hitnet_login.plist").to_string();
    let service_str = service_str.replace("{}", make_exec_content(user_id, password));

    let service_file = fs::File::create(PLIST_PATH);
    if let Err(err) = service_file {
        eprintln!("{err}");
        eprintln!("try this command with prefix 'sudo ' if you not superuser");
        return Err(())
    }
    let mut service_file = service_file.unwrap();
    let metadata = service_file.metadata().expect("metadata read error");
    let mut permission = metadata.permissions();
    permission.set_mode(0o700);
    service_file.set_permissions(permission).expect("permission write error");

    writeln!(service_file, "{}", service_str).expect("service file write failed");


    let _service_stop_cmd = Command::new("launchctl").arg("stop").arg("io.gitlab.aa20001.hitnet_login").status(); 
    let _service_unload_cmd = Command::new("launchctl").arg("unload").arg("io.gitlab.aa20001.hitnet_login").status(); 

    let _service_start_cmd = Command::new("launchctl").arg("start").arg("io.gitlab.aa20001.hitnet_login").status(); 
    let _service_load_cmd = Command::new("launchctl").arg("load").arg("io.gitlab.aa20001.hitnet_login").status(); 

    Ok(())
}

pub fn down() -> Result<(), ()> {
    let _service_stop_cmd = Command::new("launchctl").arg("stop").arg("io.gitlab.aa20001.hitnet_login").status(); 
    let _service_unload_cmd = Command::new("launchctl").arg("unload").arg("io.gitlab.aa20001.hitnet_login").status(); 

    if Path::new(PLIST_PATH).exists() {
        fs::remove_file(PLIST_PATH).expect("failed to remove plist");
    }
    Ok(())
}