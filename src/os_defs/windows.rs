use std::borrow::Cow;


fn make_exec_content(user_id: &str, password: &str) -> Result<String, xml::writer::Error> {
    use xml::writer::{EmitterConfig, XmlEvent};
    use shell_escape::windows::escape;
    use std::env;

    let escaped_user_id = escape(Cow::Borrowed(user_id));
    let escaped_password = escape(Cow::Borrowed(password));

    let mut out = Vec::new();
    let mut e = EmitterConfig::new().perform_indent(false).write_document_declaration(false).create_writer(&mut out);
    e.write(XmlEvent::start_element("Command"))?;
    e.write(XmlEvent::characters(env::current_exe().unwrap().to_str().unwrap()))?;
    e.write(XmlEvent::end_element())?;
    e.write(XmlEvent::start_element("Arguments"))?;
    e.write(XmlEvent::characters(&format!("login -u {escaped_user_id} -p {escaped_password}")))?;
    e.write(XmlEvent::end_element())?;
    let xml_data = String::from_utf8(out).expect("xml encoding error");
    return Ok(xml_data)
}


pub fn setup(user_id: &str, password: &str) -> Result<(), ()> {
    use windows::{core::{BSTR, VARIANT}, Win32::{
        Foundation::S_OK,
        System::{
            Com::{CoCreateInstance, CoInitializeEx, CoInitializeSecurity, CLSCTX_INPROC_SERVER, COINIT_MULTITHREADED, RPC_C_AUTHN_LEVEL_PKT_PRIVACY, RPC_C_IMP_LEVEL_IMPERSONATE},
            TaskScheduler::{ITaskService, TaskScheduler, TASK_CREATE_OR_UPDATE, TASK_LOGON_PASSWORD}
        }
    }};
    

    unsafe {
        
        let res = CoInitializeEx(None, COINIT_MULTITHREADED);
        if res != S_OK {
            eprintln!("CoInitializeEx Failed {res}");
            return Err(())
        };

        let hr = CoInitializeSecurity(
            None,
            -1,
            None,
            None,
            RPC_C_AUTHN_LEVEL_PKT_PRIVACY,
            RPC_C_IMP_LEVEL_IMPERSONATE,
            None,
            windows::Win32::System::Com::EOLE_AUTHENTICATION_CAPABILITIES(0),
            None);
        
        if let Err(_) = hr {
            eprintln!("CoInitializeSecurity Failed");
            return Err(());
        }
        let clsid = TaskScheduler;

        let srv:Result<ITaskService, windows::core::Error> = CoCreateInstance(&clsid, None, CLSCTX_INPROC_SERVER);

        if let Err(err) = srv {
            eprintln!("CoCreateInstance Failed");
            eprintln!("{err}");
            return Err(())
        }

        let srv = srv.unwrap();

        let con = srv.Connect(&VARIANT::from(""),& VARIANT::from(""), &VARIANT::from(""),& VARIANT::from(""));
        if let Err(err) = con {
            eprintln!("Connection Failed");
            eprintln!("{err}");
            return Err(())
        }

        let folder = srv.GetFolder(&BSTR::from("\\"));

        if let Err(err) = folder {
            eprintln!("GetFolder Failed");
            eprintln!("{err}");
            return Err(())
        }
        let folder = folder.unwrap();

        let user = std::env::var("username").expect("failed to get current user");
        println!("configurating Task as {user}");
        let xml = include_str!("../resources/task.xml");
        let xml1 = xml.to_string().replace("{}", &make_exec_content(user_id, password).expect("xml output error"));

        let password = rpassword::prompt_password("enter login user password: ").expect("password read error");
        let task = folder.RegisterTask(&BSTR::from(""), &BSTR::from(xml1), TASK_CREATE_OR_UPDATE.0, &VARIANT::from(BSTR::from(user)), &VARIANT::from(BSTR::from(password)), TASK_LOGON_PASSWORD, None);
        
        if let Err(err) = task {
            eprintln!("RegisterTask Failed");
            eprintln!("{err}");
            return Err(())
        }

        let _ = task.unwrap();
    }

    Ok(())
}

pub fn down() -> Result<(), ()> {
    use windows::{core::{BSTR, VARIANT}, Win32::{
        Foundation::S_OK,
        System::{
            Com::{CoCreateInstance, CoInitializeEx, CoInitializeSecurity, CLSCTX_INPROC_SERVER, COINIT_MULTITHREADED, RPC_C_AUTHN_LEVEL_PKT_PRIVACY, RPC_C_IMP_LEVEL_IMPERSONATE},
            TaskScheduler::{ITaskService, TaskScheduler}
        }
    }};

unsafe {
    
    let res = CoInitializeEx(None, COINIT_MULTITHREADED);
    if res != S_OK {
        eprintln!("CoInitializeEx Failed {res}");
        return Err(())
    };

    let hr = CoInitializeSecurity(
        None,
        -1,
        None,
        None,
        RPC_C_AUTHN_LEVEL_PKT_PRIVACY,
        RPC_C_IMP_LEVEL_IMPERSONATE,
        None,
        windows::Win32::System::Com::EOLE_AUTHENTICATION_CAPABILITIES(0),
        None);
    
    if let Err(_) = hr {
        eprintln!("CoInitializeSecurity Failed");
        return Err(());
    }
    let clsid = TaskScheduler;

    let srv:Result<ITaskService, windows::core::Error> = CoCreateInstance(&clsid, None, CLSCTX_INPROC_SERVER);

    if let Err(err) = srv {
        eprintln!("CoCreateInstance Failed");
        eprintln!("{err}");
        return Err(())
    }

    let srv = srv.unwrap();

    let con = srv.Connect(&VARIANT::from(""),& VARIANT::from(""), &VARIANT::from(""),& VARIANT::from(""));
    if let Err(err) = con {
        eprintln!("Connection Failed");
        eprintln!("{err}");
        return Err(())
    }

    let folder = srv.GetFolder(&BSTR::from("\\"));

    if let Err(err) = folder {
        eprintln!("GetFolder Failed");
        eprintln!("{err}");
        return Err(())
    }
    let folder = folder.unwrap();

    let _ = folder.DeleteTask(&BSTR::from("HITNET_LOGIN"), 0);
}

Ok(())
}