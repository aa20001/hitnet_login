use std::process::ExitCode;
use std::env;
use clap::{Parser, Subcommand};
use reqwest::StatusCode;
mod os_defs;
use self::os_defs::*;

#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Cli {

    /// Turn debugging information on
    #[arg(short, long, action = clap::ArgAction::Count)]
    debug: u8,

    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// does testing things
    Setup {
        /// lists test values
        #[arg(short, long)]
        user_id: String,
        
        #[arg(short, long)]
        password: Option<String>,

        #[arg(long)]
        password_from_env: bool,
    },

    Login {
        /// lists test values
        #[arg(short, long)]
        user_id: String,

        #[arg(short, long)]
        password: Option<String>,

        #[arg(long)]
        password_from_env: bool,
    },

    Down {}
}


fn login(user_id: &str, password: &str) -> Result<(), ()> {
    #[cfg(target_os = "windows")]
    {
        // let userprofile = env::var("USERPROFILE").unwrap();
        // let mut file = std::fs::File::create(format!("{userprofile}\\test.log")).unwrap();
        // writeln!(file, "user_id: {user_id}\npassword: {pass}").unwrap();
    }

    let client = reqwest::blocking::Client::new();
    
    match client.post("http://na.cc.it-hiroshima.ac.jp/cgi-bin/Login.cgi").form(&[("UID", user_id), ("PWD", password)]).send() {
        Err(error) => {
            eprintln!("{error}");
            return Err(());
        }
        Ok(resp) => {
            match resp.status() {
                StatusCode::OK => {
                    return Ok(());
                }
                els => {
                    let url = resp.url();
                    println!("{url}: {els}");
                    return Err(());
                }
            }
        }
    }
}


fn try_update() {
    
}

fn main() -> ExitCode {

    let args = Cli::parse();
    match args.command {
        Commands::Login { user_id, mut password, password_from_env } => {
            if let Option::None = password {
                if let Ok(pass) = rpassword::prompt_password("password: ") {
                    password = Some(pass)
                } else {
                    return ExitCode::FAILURE;
                }
            } else if let Some(pass) = password {
                if password_from_env {
                    if let Ok(pass) = env::var(pass) {
                        password = Some(pass)
                    } else {
                        println!("no environment variable");
                        return ExitCode::FAILURE;
                    }
                } else {
                    password = Some(pass);
                }
            } else {
                println!("no password method");
                return ExitCode::FAILURE;
            }

            if let Some(pass) = password {
                
                match login(&user_id, &pass) {
                    Ok(_) => {
                        return ExitCode::SUCCESS;
                    }
                    Err(_) => {
                        return ExitCode::FAILURE;
                    }
                };
            }
        }
        Commands::Setup { user_id, mut password , password_from_env} => {
            if let Option::None = password {
                if let Ok(pass) = rpassword::prompt_password("password: ") {
                    password = Some(pass)
                } else {
                    return ExitCode::FAILURE;
                }
            } else if let Some(pass) = password {
                if password_from_env {
                    if let Ok(pass) = env::var(pass) {
                        password = Some(pass)
                    } else {
                        println!("no environment variable");
                        return ExitCode::FAILURE;
                    }
                } else {
                    password = Some(pass);
                }
            } else {
                println!("no password method");
                return ExitCode::FAILURE;
            }

            if let Some(pass) = password {
                match setup(&user_id, &pass) {
                    Ok(_) => {
                        return ExitCode::SUCCESS;
                    }
                    Err(_) => {
                        return ExitCode::FAILURE;
                    }
                };
            }

            
        },
        Commands::Down {  } => {
            match down() {
                Ok(_) => {
                    return ExitCode::SUCCESS;
                }
                Err(_) => {
                    return ExitCode::FAILURE;
                }
            };
        }
    }

    return ExitCode::FAILURE;
}
