# HITNET-LOGIN
The software provides an easy method for captive portal authentication when connected to HITNET via Ethernet.  
HITNETへEthernetで接続した際に必要となるキャプティブポータルによる認証を簡単に行う方法を提供します  

---

## Features(機能)

* [x] Authentication (認証)
* [x] Continuos Authentication using TaskScheduler on Windows (Windowsでのタスクスケジューラを使用した継続的な認証)
* [ ] Continuos Authentication using Systemd on Linux (LinuxでのSystemdを使用した継続的な認証)
* [ ] Continuos Authentication using Launchd on macos (macosでのLaunchdを使用した継続的な認証)

## Installations(インストール方法)

### Windows

```powershell
mkdir ~/.bin
Invoke-WebRequest "https://gitlab.com/api/v4/projects/56601544/releases/permalink/latest/downloads/windows/hitnet-login.exe" -OutFile ~/.bin/hitnet-login.exe
```

### Linux

```bash
mkdir -p ~/.bin && curl --location --output ~/.bin/hitnet-login "https://gitlab.com/api/v4/projects/56601544/releases/permalink/latest/downloads/linux/hitnet-login" && chmod +x ~/.bin/hitnet-login
```

## Basic Usage(基本的な使い方)

### Oneshot Authentication(一回だけの認証)
#### Windows
```powershell
~/.bin/hitnet-login.exe login -u "your username"
```
When you run it, you will be asked to enter the password, so enter it and press the enter key (the password you entered will not be displayed. If you make a mistake, you can press the backspace key for more characters than you entered and then reenter it)  
実行するとパスワードの入力が求められるのでパスワードを入力してエンターキーを押してください(入力したパスワードは表示されません. 間違えた場合は入力した文字数より多くbackspaceキーを押してから再入力できます)  

#### Linux

```bash
~/.bin/hitnet-login login -u "your username"
```

When you run it, you will be asked to enter the password, so enter it and press the enter key (the password you entered will not be displayed. If you make a mistake, you can press the backspace key for more characters than you entered and then reenter it)  
実行するとパスワードの入力が求められるのでパスワードを入力してエンターキーを押してください(入力したパスワードは表示されません. 間違えた場合は入力した文字数より多くbackspaceキーを押してから再入力できます)  

After you enter your HITNET password, you will be prompted to enter your current Windows user password, so type it and press the enter key (the password you entered will not be displayed. If you make a mistake, you can press the backspace key for more characters than you entered and then reenter it)  
HITNETのパスワードの入力を行うとWindowsの現在のユーザーのパスワードの入力を求められるのでパスワードを入力してエンターキーを押してください(入力したパスワードは表示されません. 間違えた場合は入力した文字数より多くbackspaceキーを押してから再入力できます)  

### Continuous Authentication(継続的な認証)
#### Windows
IMPORTANT: this method have some security risk. the password will be recorded to /etc/systemd/system/hitnet_login.service, which can read by superuser or user of command execute. for example , if you don't using disk encryption, the password can read by other persons when the computer was steeled or the computer was cracked  
重要: この方法にはセキュリティ上のリスクが伴います。 パスワードはタスクスケジューラに記録され、少なくともスーパーユーザーまたはコマンド実行ユーザーが読み取ることができます(他のユーザーが読み取れるかは未検証)。たとえば、ディスク暗号化を使用していない場合、コンピュータが盗まれたり、コンピュータがクラッキングされたりしたときに、パスワードが他人に読み取られる可能性があります。  

```powershell
~/.bin/hitnet-login setup -u "your username"
```

When you run it, you will be asked to enter the HITNET password, so enter it and press the enter key (the password you entered will not be displayed. If you make a mistake, you can press the backspace key for more characters than you entered and then reenter it)  
実行するとHITNETのパスワードの入力が求められるのでパスワードを入力してエンターキーを押してください(入力したパスワードは表示されません. 間違えた場合は入力した文字数より多くbackspaceキーを押してから再入力できます)  

After you enter it, you will be asked to enter the windows user password, so enter it and press the enter key (the password you entered will not be displayed. If you make a mistake, you can press the backspace key for more characters than you entered and then reenter it)  
実行するとWindowsのユーザーのパスワードの入力が求められるのでパスワードを入力してエンターキーを押してください(入力したパスワードは表示されません. 間違えた場合は入力した文字数より多くbackspaceキーを押してから再入力できます)  

#### Linux

IMPORTANT: this method have some security risk. the password will be recorded to /etc/systemd/system/hitnet_login.service, which can read by superuser or user of command execute. for example , if you don't using disk encryption, the password can read by other persons when the computer was steeled or the computer was cracked  
重要: この方法にはセキュリティ上のリスクが伴います。 パスワードは /etc/systemd/system/hitnet_login.service に記録され、スーパーユーザーまたはコマンド実行ユーザーが読み取ることができます。たとえば、ディスク暗号化を使用していない場合、コンピュータが盗まれたり、コンピュータがクラッキングされたりしたときに、パスワードが他人に読み取られる可能性があります。  

```bash
sudo ~/.bin/hitnet-login setup -u "your username"
```

When you run it, you will be asked to enter the password, so enter it and press the enter key (the password you entered will not be displayed. If you make a mistake, you can press the backspace key for more characters than you entered and then reenter it)  
実行するとパスワードの入力が求められるのでパスワードを入力してエンターキーを押してください(入力したパスワードは表示されません. 間違えた場合は入力した文字数より多くbackspaceキーを押してから再入力できます)  



### Continuous Authentication(継続的な認証の解除)
#### Windows
If you want to remove continuous authentication from your computer, run the following command  
コンピュータから継続な認証を削除する場合は、次のコマンドを実行します


```powershell
~/.bin/hitnet-login down
```

#### Linux
If you want to remove continuous authentication from your computer, run the following command  
コンピュータから継続な認証を削除する場合は、次のコマンドを実行します

```bash
sudo ~/.bin/hitnet-login down
```




### Advanced Usage(高度な使い方)
#### Password from Environments Variable(環境変数からパスワードを入力する)
```bash
sudo ~/.bin/hitnet-login setup -u "your username" -p ENV_NAME --password-from-env
```
You can enter the password from an environment variable. This example shows the use of the password from the environment variable `ENV_NAME`.  
環境変数からパスワードを入力できます。 この例では、環境変数 `ENV_NAME` からのパスワードの使用を示しています。  

#### Password from Arguments(引数からパスワードを入力する)

IMPORTANT: this method have some security risk. for example, the password will be recorded by shell, /var/log/secure and more. for example , if you don't using disk encryption, the password can read by other persons when the computer was steeled or the computer was cracked  
重要: この方法にはセキュリティ上のリスクが伴います。 たとえば、パスワードはシェル、/var/log/secure などによって記録されます。たとえば、ディスク暗号化を使用していない場合、コンピュータが盗まれたり、コンピュータがクラッキングされたりしたときに、パスワードが他人に読み取られる可能性があります。  

```bash
sudo ~/.bin/hitnet-login setup -u "your username" -p "your password"
```
You can enter the password from an arguments. This example shows the password is `your password`.  
引数からパスワードを入力できます。 この例では、パスワードが`your passworc`であることを示しています。  






